create-kind-cluster:
	bash scripts/local/create-kind-cluster.sh

delete-kind-cluster:
	bash scripts/local/delete-kind-cluster.sh

deploy-local:
	bash scripts/local/deploy.sh

test-local:
	bash scripts/local/test.sh

opa-test-cli:
	docker run -it --rm -v "$(shell pwd):/workdir" -w /workdir --entrypoint=ash openpolicyagent/opa:0.55.0-static-debug

opa-test-policies:
	docker run --rm -v "$(shell pwd):/workdir" -w /workdir --entrypoint=ash openpolicyagent/opa:0.55.0-static-debug -c "opa test -v policies/chart/policies/"
