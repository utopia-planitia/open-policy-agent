#!/bin/sh

set -eux

if set +o | grep -Fq 'set +o pipefail'; then
  set -o pipefail
fi

kind create cluster --name opa || true
