#!/bin/sh

set -eux

if set +o | grep -Fq 'set +o pipefail'; then
  set -o pipefail
fi

kind delete cluster --name opa || true
