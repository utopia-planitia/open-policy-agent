#!/bin/sh

set -eux

if set +o | grep -Fq 'set +o pipefail'; then
  set -o pipefail
fi

helmfile --kube-context=kind-opa sync \
  --file opa/helmfile.yaml \
  --set certManager.enabled=false \
  --set deploymentStrategy.type=Recreate \
  --set podDisruptionBudget.enabled=false \
  --set replicas=1 \
  --wait

helmfile --kube-context=kind-opa sync \
  --file policies/helmfile.yaml \
  --wait
