#!/bin/sh

set -eux

if set +o | grep -Fq 'set +o pipefail'; then
  set -o pipefail
fi

helmfile --kube-context=kind-opa sync --file tests/helmfile.yaml
helmfile --kube-context=kind-opa test --file tests/helmfile.yaml --logs
