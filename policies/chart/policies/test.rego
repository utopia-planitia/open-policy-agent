package kubernetes.admission

import future.keywords
import data.kubernetes.ingresses

# the 'incomming' request
input_duplicated_url_mock := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "spec": {
                "rules": [
                    {
                        "host": "opa-test.domain.tld"
                    }
                ]
            }
        },
        "namespace": "opa-test2"
    }
}

input_unique_url_mock := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "spec": {
                "rules": [
                    {
                        "host": "opa-test-unique.domain.tld"
                    }
                ]
            }
        },
        "namespace": "opa-test3"
    }
}

# the 'existing' data in k8s
data_mock_ingresses := {
    "namespace-name-opa-test1": {
        "ingress-name-opa-test1": {
            "spec": {
                "rules": [
                    {
                        "host": "opa-test.domain.tld"
                    }
                ]
            }
        }
    }
}

test_conflicted_ingress if {
    violations := deny with input as input_duplicated_url_mock
        with data.kubernetes.ingresses as data_mock_ingresses
    count(violations) > 0
}
test_unique_ingress if {
    violations := deny with input as input_unique_url_mock
        with data.kubernetes.ingresses as data_mock_ingresses
    count(violations) == 0
}
