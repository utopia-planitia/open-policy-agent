package kubernetes.admission

import future.keywords.contains
import future.keywords.if
import future.keywords.every

is_ingress if {
    input.request.kind.kind == "Ingress"
}

is_allowed_operation if {
    input.request.operation == "CREATE"
}
is_allowed_operation if {
    input.request.operation == "UPDATE"
}

deny_larger_than_63_chars_for_subdomains contains msg if {
    is_ingress
    is_allowed_operation

    input_host := input.request.object.spec.rules[_].host

    subdomains := split(input_host, ".")
    subdomain := subdomains[_]
    count(subdomain) > 63

    ingressName := input.request.object.metadata.name
    msg := sprintf("Host for ingress (ingress name:'%v') has subdomain (domain label) with more than 63 characters: '%v' (checked domain: '%v')", [ingressName, subdomain, input_host])
}

## tests: #####################################################################

# the 'incoming' request
input_mock_for_ingress_with_subdomains_less_or_equal_than_63_chars_create := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "metadata": {
                "name": "ingress-subdomain-length-ok",
            },
            "spec": {
                "rules": [
                    {
                        "host": "xxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyy123.domain.tld"
                    }
                ]
            }
        }
    }
}
input_mock_for_ingress_with_subdomains_less_or_equal_than_63_chars_update := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "UPDATE",
        "object": {
            "metadata": {
                "name": "ingress-subdomain-length-ok",
            },
            "spec": {
                "rules": [
                    {
                        "host": "xxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyy123.domain.tld"
                    }
                ]
            }
        }
    }
}
input_mock_for_ingress_with_subdomains_larger_than_63_chars_create := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "metadata": {
                "name": "ingress-subdomain-length-bad",
            },
            "spec": {
                "rules": [
                    {
                        "host": "xxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyy1234.domain.tld"
                    }
                ]
            }
        }
    }
}
input_mock_for_ingress_with_subdomains_larger_than_63_chars_create_update := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "UPDATE",
        "object": {
            "metadata": {
                "name": "ingress-subdomain-length-bad",
            },
            "spec": {
                "rules": [
                    {
                        "host": "xxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyy1234.domain.tld"
                    }
                ]
            }
        }
    }
}
input_mock_operation_delete := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "DELETE",
        "object": {
            "metadata": {
                "name": "ingress-subdomain-length-bad-but-allowed-delete",
            },
            "spec": {
                "rules": [
                    {
                        "host": "xxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyyxxxxxxxxxxyyyyyyyyyy1234.domain.tld"
                    }
                ]
            }
        }
    }
}
input_mock_another_kind := {
    "request": {
        "kind": {
            "kind": "AnotherKind"
        }
    }
}

test_deny_larger_create if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_for_ingress_with_subdomains_larger_than_63_chars_create
    # uncomment the following for debugging test executions:
    # print("violations:", violations)
    # print("violations:", count(violations))
    count(violations) > 0

    every msg in violations {
        contains(msg, input_mock_for_ingress_with_subdomains_larger_than_63_chars_create.request.object.metadata.name)
    }
}
test_deny_larger_update if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_for_ingress_with_subdomains_larger_than_63_chars_create_update
    # uncomment the following for debugging test executions:
    # print("violations:", violations)
    # print("violations:", count(violations))
    count(violations) > 0

    every msg in violations {
        contains(msg, input_mock_for_ingress_with_subdomains_larger_than_63_chars_create.request.object.metadata.name)
    }
}
test_not_deny_less_create if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_for_ingress_with_subdomains_less_or_equal_than_63_chars_create
    # uncomment the following for debugging test executions:
    # print("violations:", violations)
    # print("violations:", count(violations))
    count(violations) == 0
}
test_not_deny_less_update if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_for_ingress_with_subdomains_less_or_equal_than_63_chars_update
    # uncomment the following for debugging test executions:
    # print("violations:", violations)
    # print("violations:", count(violations))
    count(violations) == 0
}
test_allow_delete_operation if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_operation_delete
    count(violations) == 0
}
test_allow_other_kinds if {
    violations := deny_larger_than_63_chars_for_subdomains with input as input_mock_another_kind
    count(violations) == 0
}
