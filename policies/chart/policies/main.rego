package system

import data.kubernetes.admission

deny[msg] {
    admission.deny[msg]
}
deny[msg] {
    admission.deny_larger_than_63_chars_for_subdomains[msg]
}

main := {
  "apiVersion": "admission.k8s.io/v1",
  "kind": "AdmissionReview",
  "response": response,
}

default uid := ""

uid := input.request.uid

response := {
    "allowed": false,
    "uid": uid,
    "status": {
        "message": reason,
    },
} {
    reason = concat(", ", deny)
    reason != ""
}

else := {"allowed": true, "uid": uid}
