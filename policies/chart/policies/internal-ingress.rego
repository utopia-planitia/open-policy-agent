package kubernetes.admission

import future.keywords.contains
import future.keywords.if
import future.keywords.every

operations := {"CREATE", "UPDATE"}

deny[msg] {
    input.request.kind.kind == "Ingress"
    operations[input.request.operation]
    host := input.request.object.spec.rules[_].host
    endswith(host, ".svc.cluster.local")
    msg := sprintf("ingress host %q is internal and therefore invalid", [host])
}

input_mock_internal_ingress := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "spec": {
                "rules": [
                    {
                        "host": "opa-test.svc.cluster.local"
                    }
                ]
            }
        },
        "namespace": "opa-test"
    }
}
input_mock_non_internal_ingress := {
    "request": {
        "kind": {
            "kind": "Ingress"
        },
        "operation": "CREATE",
        "object": {
            "spec": {
                "rules": [
                    {
                        "host": "opa-test.example"
                    }
                ]
            }
        },
        "namespace": "opa-test"
    }
}

test_deny_internal_ingress_basic if {
    deny with input as input_mock_internal_ingress
}
test_deny_internal_ingress if {
    violations := deny with input as input_mock_internal_ingress
    # print("violations:", violations)
    count(violations) > 0
    every msg in violations {
        contains(msg, "is internal and therefore invalid")
    }
}
test_deny_internal_ingress if {
    violations := deny with input as input_mock_non_internal_ingress
    # print("violations:", violations)
    count(violations) == 0
}
