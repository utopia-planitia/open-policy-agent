apiVersion: v1
kind: ConfigMap
metadata:
  name: rules
  labels:
    openpolicyagent.org/policy: rego
data:
  {{ (.Files.Glob "policies/*").AsConfig | indent 2 | trim }}
