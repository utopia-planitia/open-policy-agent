# Open Policy Agent

This repo deploys [Open Policy Agent](https://www.openpolicyagent.org/docs/).

## Policy example:

https://www.openpolicyagent.org/docs/latest/kubernetes-admission-control/

## Local testing

Requirements:

- kind
- kubectl
- helmfile

```sh
# run once
make create-kind-cluster

# run once, but repeat this command when you make changes to opa or policies
make deploy-local

# run the tests as often as you want
make test-local

# cleanup
make delete-kind-cluster
```
