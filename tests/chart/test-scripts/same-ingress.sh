#!/bin/bash
set -euxo pipefail

# prepare namespace
kubectl delete ns opa-test --ignore-not-found=true
kubectl create ns opa-test
kubectl delete ns opa-test2 --ignore-not-found=true
kubectl create ns opa-test2

sleep 10

# create ingress
kubectl -n opa-test apply -f ingress-ok.yaml

sleep 1

# counterexample
set +e
if kubectl -n opa-test2 apply -f ingress-same.yaml; then
  echo 'Same ingress was created. This should not happen.'
  exit 1
fi
set -e

# cleanup
kubectl delete ns opa-test --ignore-not-found=true
kubectl delete ns opa-test2 --ignore-not-found=true
