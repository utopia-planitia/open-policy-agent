#!/bin/bash

# Description:
#  - Test creating ingress subdomains with more than 63 characters should be denied (as those would be invalid)
#  - Test updating ingress subdomains with more than 63 characters should be denied (as those would be invalid)
#  - Test creating ingress subdomains with less or equal 63 characters should be allowed

set -euxo pipefail

NAMESPACE=opa-test-ingress-subdomain-length

# prepare namespace
kubectl delete ns ${NAMESPACE} --ignore-not-found=true
kubectl create ns ${NAMESPACE}

sleep 10

# test create ok-ingress
kubectl -n ${NAMESPACE} apply -f ingress-subdomain-length-ok.yaml

sleep 1

# test create bad-ingress is denied
if kubectl -n ${NAMESPACE} apply -f ingress-subdomain-length-bad.yaml; then
  echo 'An ingress with a subdomain with more than 63 characters was created. This should not happen.'
  exit 1
fi

sleep 1

# test update ingress with bad patch is denied
if kubectl -n ${NAMESPACE} patch -f ingress-subdomain-length-ok.yaml \
  --type=json --patch='[{"op": "replace", "path": "/spec/rules/0/host", "value":"1111111111222222222233333333334444444444555555555566666666661234.test"}]'; then
  echo 'An ingress with a subdomain with more than 63 characters was updated. This should not happen.'
  exit 1
fi

# cleanup
kubectl delete ns ${NAMESPACE} --ignore-not-found=true
