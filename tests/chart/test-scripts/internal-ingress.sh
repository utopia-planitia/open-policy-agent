#!/bin/bash
set -euxo pipefail

# prepare namespace
kubectl delete ns opa-test-internal --ignore-not-found=true
kubectl create ns opa-test-internal

sleep 10

# create ingress
kubectl -n opa-test-internal apply -f ingress-ok.yaml

sleep 1

# counterexample
set +e
if kubectl -n opa-test-internal apply -f ingress-bad.yaml; then
  echo 'Internal ingress was created. This should not happen.'
  exit 1
fi
set -e

# cleanup
kubectl delete ns opa-test-internal --ignore-not-found=true
